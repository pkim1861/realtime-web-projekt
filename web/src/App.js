import React, { useEffect } from "react";
import NavBar from "./components/navigation/NavBar";
import Routing from "./components/Routing";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { io } from "socket.io-client";
import { useDispatch } from "react-redux";
import { setSocket } from "./stores/socketStore";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    const newSocket = io("http://localhost:8000");
    dispatch(setSocket(newSocket));
    return () => newSocket.close();
  }, [dispatch]);

  return (
    <div className="App">
      <NavBar />
      <Routing />
    </div>
  );
}

export default App;
