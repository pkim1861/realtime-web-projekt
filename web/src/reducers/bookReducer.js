import { createSlice } from "@reduxjs/toolkit";
// Slice
export const bookSlice = createSlice({
  name: "book",
  initialState: {
    loading: false,
    error: false,
    books: null,
    comments: null,
    selectedBook: null,
  },
  reducers: {
    uploadNewBookStart: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    uploadNewBookSuccess: (state, action) => {
      state.loading = false;
      state.error = false;
    },
    uploadNewBookFail: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    getBooksStart: (state, action) => {
      state.loading = true;
      state.error = false;
      state.books = null;
    },
    getBooksSuccess: (state, action) => {
      state.loading = false;
      state.error = false;
      state.books = action.payload.data;
    },
    getBooksFail: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    setAsFavoriteSuccess: (state, action) => {
      let i = 0;
      while (
        state.books[i].id !== action.payload.bookId &&
        i < state.books.length
      ) {
        i++;
      }
      if (state.books[i].id === action.payload.bookId) {
        if (action.payload.same) {
          state.books[i].favorite = action.payload.favorite;
        }
        state.books[i].favoriteNr = action.payload.favoriteNr;
      }
    },
    setAsSelectedBook: (state, action) => {
      if (action.payload.bookId === null) state.selectedBook = null;
      else {
        state.selectedBook = action.payload;
      }
    },
  },
});

export const {
  uploadNewBookStart,
  uploadNewBookFail,
  uploadNewBookSuccess,
  getBooksStart,
  getBooksSuccess,
  getBooksFail,
  getCommentsStart,
  getCommentsSuccess,
  getCommentsFail,
  setAsFavoriteSuccess,
  setAsSelectedBook,
} = bookSlice.actions;

export default bookSlice.reducer;
