import { createSlice } from "@reduxjs/toolkit";
const initialUser = localStorage.getItem("user")
  ? {
      token: localStorage.getItem("token"),
      username: localStorage.getItem("user"),
      userId: localStorage.getItem("userId"),
    }
  : null;

// Slice
export const userSlice = createSlice({
  name: "user",
  initialState: {
    user: initialUser,
    loginError: null,
    signUpError: null,
    logoutError: null,
    loading: false,
    users: null,
  },
  reducers: {
    signUpStart: (state, action) => {
      state.loginError = null;
      state.signUpError = null;
      state.loading = true;
    },
    signUpSuccess: (state, action) => {
      state.loginError = null;
      state.signUpError = null;
      state.loading = false;
    },
    signUpFail: (state, action) => {
      state.loginError = null;
      state.signUpError = action.payload;
      state.loading = false;
    },
    loginStart: (state, action) => {
      state.loginError = null;
      state.signUpError = null;
      state.loading = true;
    },
    loginSuccess: (state, action) => {
      state.loginError = null;
      state.signUpError = null;
      state.loading = false;
      state.user = action.payload;
      localStorage.setItem("token", action.payload.token);
      localStorage.setItem("user", action.payload.username);
      localStorage.setItem("userId", action.payload.userId);
    },
    loginFail: (state, action) => {
      state.loginError = action.error;
      state.signUpError = null;
      state.loading = false;
      state.user = null;
    },
    logoutStart: (state, action) => {
      state.logoutError = null;
      state.loading = true;
    },
    logoutSuccess: (state, action) => {
      state.logoutError = null;
      state.loading = false;
      state.user = null;
      localStorage.removeItem("token");
      localStorage.removeItem("user");
    },
    logoutFail: (state, action) => {
      state.logouterror = action.error;
      state.loading = false;
    },
    getAllUsersStart: (state, action) => {
      state.loading = true;
      state.users = null;
    },
    getAllUsersSuccess: (state, action) => {
      state.loading = false;
      state.users = action.payload.data;
    },
    getAllUsersFail: (state, action) => {
      state.loading = false;
      state.users = null;
    },
  },
});

export const {
  signUpStart,
  signUpFail,
  signUpSuccess,
  loginStart,
  loginFail,
  loginSuccess,
  logoutStart,
  logoutFail,
  logoutSuccess,
  getAllUsersStart,
  getAllUsersSuccess,
  getAllUsersFail,
} = userSlice.actions;

export default userSlice.reducer;
