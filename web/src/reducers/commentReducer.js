import { createSlice } from "@reduxjs/toolkit";
// Slice
export const bookSlice = createSlice({
  name: "comment",
  initialState: {
    loading: false,
    error: false,
    comments: null,
  },
  reducers: {
    getCommentsStart: (state, action) => {
      state.loading = true;
      state.error = false;
      state.comments = null;
    },
    getCommentsSuccess: (state, action) => {
      state.loading = false;
      state.error = false;
      state.comments = action.payload.data;
    },
    getCommentsFail: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    updateCommentSuccess: (state, action) => {
      let contains = false;
      for (let i = 0; i < state.comments.length; i += 1) {
        if (state.comments[i].id === action.payload.id) {
          contains = true;
        }
      }
      if (!contains) state.comments.unshift(action.payload);
    },
  },
});
export const {
  getCommentsStart,
  getCommentsSuccess,
  getCommentsFail,
  updateCommentSuccess,
} = bookSlice.actions;

export default bookSlice.reducer;
