import { createSlice } from "@reduxjs/toolkit";
// Slice
export const chatSlice = createSlice({
  name: "chat",
  initialState: {
    loading: false,
    error: false,
    messages: null,
  },
  reducers: {
    getChatsStart: (state, action) => {
      state.loading = true;
      state.error = false;
      state.messages = null;
    },
    getChatsSuccess: (state, action) => {
      state.loading = false;
      state.error = false;
      state.messages = action.payload.data;
    },
    getChatsFail: (state, action) => {
      state.loading = false;
      state.error = false;
      state.messages = null;
    },
    updateChatMessagesR: (state, action) => {
      let contains = false;
      for (let i = 0; i < state.messages.length; i++) {
        if (state.messages[i].id === action.payload.id) contains = true;
      }
      if (!contains) {
        state.messages.unshift(action.payload);
      }
    },
  },
});

export const {
  getChatsStart,
  getChatsSuccess,
  getChatsFail,
  updateChatMessagesR,
} = chatSlice.actions;

export default chatSlice.reducer;
