import { createSlice } from "@reduxjs/toolkit";

// Slice
export const socketSlice = createSlice({
  name: "socket",
  initialState: {
    socket: null,
  },
  reducers: {
    settingSocket: (state, action) => {
      state.socket = action.payload;
    },
  },
});

export const { settingSocket } = socketSlice.actions;

export default socketSlice.reducer;
