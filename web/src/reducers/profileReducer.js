import { createSlice } from "@reduxjs/toolkit";

// Slice
export const profileSlice = createSlice({
  name: "profile",
  initialState: {
    userData: null,
    books: null,
    error: false,
    loading: false,
  },
  reducers: {
    getProfileDataStart: (state, action) => {
      state.userData = null;
      state.books = null;
      state.error = false;
      state.loading = true;
    },
    getProfileDataSuccess: (state, action) => {
      state.userData = {
        firstName: action.payload.firstName,
        lastName: action.payload.lastName,
        profilePicture: action.payload.image,
      };

      state.books = action.payload.books;
      state.error = null;
      state.loading = false;
    },
    getProfileDataFail: (state, action) => {
      state.userData = null;
      state.books = null;
      state.error = action.payload;
      state.loading = false;
    },
  },
});

export const {
  getProfileDataStart,
  getProfileDataSuccess,
  getProfileDataFail,
} = profileSlice.actions;

export default profileSlice.reducer;
