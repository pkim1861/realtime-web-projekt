import { createSlice } from "@reduxjs/toolkit";
// Slice
export const notificationSlice = createSlice({
  name: "notification",
  initialState: {
    loading: false,
    error: false,
    notifications: null,
    notSeenNotificationNr: 0,
  },
  reducers: {
    getNotificationsStart: (state, action) => {
      state.loading = true;
      state.error = false;
      state.notifications = null;
      state.notSeenNotificationNr = 0;
    },
    getNotificationsSuccess: (state, action) => {
      state.loading = false;
      state.error = false;
      state.notifications = action.payload.data;

      //setting notification nr
      for (let i = 0; i < action.payload.data.length; i++) {
        if (!action.payload.data[i].seen) {
          state.notSeenNotificationNr++;
        }
      }
    },
    getNotificationsFail: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    updateNotificationsSuccess: (state, action) => {
      let contains = false;
      for (let i = 0; i < state.notifications.length; i++) {
        if (state.notifications[i].id === action.payload.id) contains = true;
        i++;
      }
      if (!contains) {
        state.notifications.unshift(action.payload);
        state.notSeenNotificationNr++;
      }
    },
    setNotificationSeenStart: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    setNotificationSeenSuccess: (state, action) => {
      state.loading = false;
      state.error = false;
    },
    setNotificationSeenFail: (state, action) => {
      state.loading = false;
      state.error = true;
    },
    setNotificationsLocallySeenR: (state, action) => {
      for (let i = 0; i < state.notifications.length; i++) {
        state.notifications[i].seen = true;
      }
      state.notSeenNotificationNr = 0;
    },
  },
});
export const {
  getNotificationsStart,
  getNotificationsSuccess,
  getNotificationsFail,
  updateNotificationsSuccess,
  setNotificationSeenStart,
  setNotificationSeenSuccess,
  setNotificationSeenFail,
  setNotificationsLocallySeenR,
} = notificationSlice.actions;

export default notificationSlice.reducer;
