import React, { useState, useEffect } from "react";
import { Modal, Row, Image, Button } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { AiOutlineMessage } from "react-icons/ai";
import { IconContext } from "react-icons";
import { getChats } from "../stores/chatStore";
import ChatModal from "./ChatModal";

function UserModal(props) {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.user.users);
  const userId = useSelector((state) => state.user.user.userId);

  const [chosenUser, setChosenUser] = useState({});
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => {
    setShow(true);
  };

  useEffect(() => {
    if (show) {
      dispatch(getChats(userId, chosenUser.id));
    }
  }, [show, chosenUser.id, dispatch, userId]);

  return (
    <Modal
      show={props.show}
      onHide={props.handleClose}
      animation={false}
      className="modal"
      scrollable={true}
      size="lg"
    >
      <Modal.Header closeButton>
        <Modal.Title>Users</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          {users !== null &&
            users.map((user, i) => {
              return (
                <>
                  <div key={i}>
                    <Image
                      src={`http://localhost:8000/${user.image}`}
                      roundedCircle="true"
                      style={{
                        width: "3rem",
                      }}
                    />
                    {user.firstName} {user.lastName}
                    <Button
                      variant="light"
                      size="sm"
                      onClick={() => {
                        setChosenUser(user);
                        handleShow();
                      }}
                    >
                      <IconContext.Provider
                        value={{
                          color: "#A27C5B",
                          className: "global-class-name",
                          size: "25px",
                          margin: "1000px",
                        }}
                      >
                        <AiOutlineMessage />
                      </IconContext.Provider>
                    </Button>
                    <ChatModal
                      show={show}
                      handleClose={handleClose}
                      profileData={chosenUser}
                      id={chosenUser.id}
                    />
                  </div>
                </>
              );
            })}
        </Row>
      </Modal.Body>
    </Modal>
  );
}

UserModal.propTypes = {
  show: PropTypes.bool,
  handleClose: PropTypes.func,
};

export default UserModal;
