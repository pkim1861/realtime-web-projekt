/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { Modal, Row } from "react-bootstrap";
import ChatForm from "../components/forms/ChatForm";
import { useSelector, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { updateChatMessages } from "../stores/chatStore";

function ChatModal(props) {
  const dispatch = useDispatch();

  const chatMessages = useSelector((state) => state.chat.messages);
  const userId = useSelector((state) => state.user.user.userId);
  const socket = useSelector((state) => state.socket.socket);

  if (socket !== null) {
    socket.on("newChat", (newChat) => {
      dispatch(updateChatMessages(newChat));
    });
  }

  useEffect(() => {
    if (props.show) {
      socket.emit("onChats", userId);
    }
  }, [props.show]);

  return (
    <Modal
      show={props.show}
      onHide={props.handleClose}
      animation={false}
      className="modal"
      scrollable={true}
      size="lg"
    >
      {props.profileData !== null && (
        <>
          <Modal.Header closeButton>
            <Modal.Title>
              {props.profileData.firstName} {props.profileData.lastName}
            </Modal.Title>
          </Modal.Header>

          <Modal.Body>
            {chatMessages !== null &&
              chatMessages.map((chatMessage, i) => {
                return (
                  <Row key={i}>
                    {chatMessage.userFromFirstName}{" "}
                    {chatMessage.userFromLastName}: {chatMessage.message}
                  </Row>
                );
              })}
          </Modal.Body>
          <hr />
          <Modal.Footer class="mr-auto">
            <ChatForm id={props.id} />
            <br />
          </Modal.Footer>
        </>
      )}
    </Modal>
  );
}

ChatModal.propTypes = {
  show: PropTypes.bool,
  handleClose: PropTypes.func,
  profileData: PropTypes.object,
  id: PropTypes.string,
};

export default ChatModal;
