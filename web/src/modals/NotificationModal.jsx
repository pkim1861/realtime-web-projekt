import React from "react";
import { Modal, Row, Badge } from "react-bootstrap";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

function NotificationModal(props) {
  const notifications = useSelector(
    (state) => state.notification.notifications
  );

  return (
    <Modal
      show={props.show}
      onHide={props.handleClose}
      animation={false}
      className="modal"
      scrollable={true}
      size="lg"
    >
      <Modal.Header closeButton>
        <Modal.Title>Notifications</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          {notifications !== null &&
            notifications.map((notification, i) => {
              return (
                <div key={i}>
                  {notification.message}
                  {!notification.seen && (
                    <Badge pill bg="danger">
                      1
                    </Badge>
                  )}
                </div>
              );
            })}
        </Row>
      </Modal.Body>
    </Modal>
  );
}

NotificationModal.propTypes = {
  show: PropTypes.bool,
  handleClose: PropTypes.func,
};

export default NotificationModal;
