import React, { useEffect } from "react";
import {
  Button,
  Row,
  Col,
  Modal,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import { Image } from "react-bootstrap";
import { IconContext } from "react-icons";
import { MdFavoriteBorder, MdFavorite } from "react-icons/md";
import { useSelector, useDispatch } from "react-redux";
import { getComments, updateComments } from "../stores/commentStore";
//import { setFavorite } from "../stores/bookStore";
import { useNavigate } from "react-router-dom";
import Moment from "react-moment";
import CommentsForm from "../components/forms/CommentsForm";
import PropTypes from "prop-types";

function BookModal(props) {
  const dispatch = useDispatch();
  const comments = useSelector((state) => state.comment.comments);
  const book = props.book;
  let navigate = useNavigate();
  const socket = useSelector((state) => state.socket.socket);
  const username = useSelector((state) => state.user.user.username);

  useEffect(() => {
    socket.on("newComment", (newComment) => {
      dispatch(updateComments(newComment));
    });

    return () => {
      socket.emit("onCloseComments");
      socket.off("newComment");
    };
  }, [dispatch, socket, username]);

  useEffect(() => {
    if (props.show) dispatch(getComments(props.book.id));
  }, [dispatch, props.book.id, props.show]);

  useEffect(() => {
    if (props.show) {
      socket.emit("onComments", props.book.id);
    } else {
      socket.emit("onCloseComments");
    }
  }, [props.show, socket, props.book.id]);

  const markAsFavorite = () => {
    socket.emit("markAsFavorite", {
      bookId: props.book.id,
      username,
    });
  };

  return (
    <Modal
      show={props.show}
      onHide={props.handleClose}
      animation={false}
      className="modal"
      scrollable={true}
      size="lg"
    >
      <Modal.Header closeButton>
        <Modal.Title>
          {book.title}
          <br />
          {book.author}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col>
            <Image
              src={`http://localhost:8000/${book.image}`}
              style={{ width: "100%", height: "40vh", objectFit: "contain" }}
            />
          </Col>
          <Col>
            <Row
              as="a"
              onClick={() => {
                let id = book.userId;
                let path = `/users/${encodeURIComponent(id)}`;
                navigate(path);
              }}
            >
              <Image
                src={`http://localhost:8000/${book.userProfilePicture}`}
                roundedCircle="true"
                style={{
                  width: "5rem",
                }}
              />{" "}
              {book.userFirstname + " " + book.userLastname}
            </Row>
            <Row>
              <Col>{"Did you like this book? Add to favorites!"}</Col>
              <Col>
                <OverlayTrigger
                  placement="bottom"
                  delay={{ show: 250, hide: 400 }}
                  overlay={
                    <Tooltip>
                      {book.favorite
                        ? "You added this to your favorites"
                        : "Add to favorites"}
                    </Tooltip>
                  }
                >
                  <Button
                    variant="light"
                    onClick={() => {
                      if (!book.favorite) markAsFavorite();
                    }}
                  >
                    {book.favorite ? (
                      <IconContext.Provider
                        value={{
                          color: "#A27C5B",
                          className: "global-class-name",
                          size: "25px",
                          margin: "1000px",
                        }}
                      >
                        <MdFavorite />
                      </IconContext.Provider>
                    ) : (
                      <IconContext.Provider
                        value={{
                          color: "#A27C5B",
                          className: "global-class-name",
                          size: "25px",
                          margin: "1000px",
                        }}
                      >
                        <MdFavoriteBorder />
                      </IconContext.Provider>
                    )}
                  </Button>
                </OverlayTrigger>
              </Col>
            </Row>
          </Col>
        </Row>
        <hr />
        <Row>
          <strong>Comments</strong>
          {comments !== null &&
            comments.map((comment, i) => (
              <>
                <Row key={i}>
                  <Col>
                    {comment.commenterFirstName} {comment.commenterLastName}{" "}
                    {":"} {comment.text}
                  </Col>
                  <Col>
                    <Moment format="YYYY/MM/DD HH:mm">
                      {comment.uploadDate}
                    </Moment>
                  </Col>
                </Row>
              </>
            ))}
        </Row>
      </Modal.Body>
      <hr />
      <Modal.Footer class="mr-auto">
        <CommentsForm book={book} />
        <br />
      </Modal.Footer>
    </Modal>
  );
}

BookModal.propTypes = {
  book: PropTypes.object,
  show: PropTypes.bool,
  handleClose: PropTypes.func,
};

export default BookModal;
