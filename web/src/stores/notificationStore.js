import {
  getNotificationsStart,
  getNotificationsSuccess,
  getNotificationsFail,
  updateNotificationsSuccess,
  setNotificationSeenStart,
  setNotificationSeenSuccess,
  setNotificationSeenFail,
  setNotificationsLocallySeenR,
} from "../reducers/notificationReducer";
const axios = require("axios").default;

export const getNotifications = (userId) => {
  return (dispatch) => {
    dispatch(getNotificationsStart());
    axios
      .get(
        `http://localhost:8000/api/users/${encodeURIComponent(
          userId
        )}/notifications`,
        {
          headers: { Authorization: "Bearer " + localStorage.getItem("token") },
        }
      )
      .then((data) => {
        if (data.error !== undefined) {
          dispatch(getNotificationsFail(data.error));
        } else {
          dispatch(getNotificationsSuccess(data));
        }
      })
      .catch(() => {
        dispatch(getNotificationsFail());
      });
  };
};

export const updateNotifications = (newNotification) => {
  return (dispatch) => {
    dispatch(updateNotificationsSuccess(newNotification));
  };
};

export const setNotificationsSeen = (id) => {
  return (dispatch) => {
    dispatch(setNotificationSeenStart());
    const newData = { seen: true };
    axios
      .patch(
        `http://localhost:8000/api/users/${encodeURIComponent(
          id
        )}/notifications`,
        newData,
        {
          headers: { Authorization: "Bearer " + localStorage.getItem("token") },
        }
      )
      .then((data) => {
        if (data.error !== undefined) {
          dispatch(setNotificationSeenFail(data.error));
        } else {
          dispatch(setNotificationSeenSuccess(data));
        }
      })
      .catch(() => {
        dispatch(setNotificationSeenFail());
      });
  };
};

export const setNotificationsLocallySeen = () => {
  return (dispatch) => {
    dispatch(setNotificationsLocallySeenR());
  };
};
