import {
  uploadNewBookStart,
  uploadNewBookSuccess,
  uploadNewBookFail,
  getBooksStart,
  getBooksSuccess,
  getBooksFail,
  setAsFavoriteSuccess,
  setAsSelectedBook,
} from "../reducers/bookReducer";
const axios = require("axios").default;

export const uploadNewBook = (book, username) => {
  return (dispatch) => {
    dispatch(uploadNewBookStart());
    let data = new FormData();
    data.append("file", book.file);
    data.append("author", book.author);
    data.append("title", book.title);
    data.append("username", username.username);
    axios
      .post("http://localhost:8000/api/books", data, {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.error !== undefined) {
          dispatch(uploadNewBookFail(data.error));
        } else {
          dispatch(uploadNewBookSuccess(data));
          alert("Successful upload!");
        }
      })
      .catch((error) => {
        dispatch(uploadNewBookFail());
      });
  };
};

export const getBooks = () => {
  return (dispatch) => {
    dispatch(getBooksStart());
    axios
      .get("http://localhost:8000/api/books", {
        headers: { Authorization: "Bearer " + localStorage.getItem("token") },
      })
      .then((data) => {
        if (data.error !== undefined) {
          dispatch(getBooksFail(data.error));
        } else {
          dispatch(getBooksSuccess(data));
        }
      })
      .catch(() => {
        dispatch(getBooksFail());
      });
  };
};

export const setFavorite = (bookId, same, favorite, favoriteNr) => {
  return (dispatch) => {
    dispatch(setAsFavoriteSuccess({ bookId, same, favorite, favoriteNr }));
  };
};

export const setChosenBook = (book) => {
  return (dispatch) => {
    dispatch(setAsSelectedBook(book));
  };
};
