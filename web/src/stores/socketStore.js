import { settingSocket } from "../reducers/socketReducer";

export const setSocket = (socket) => {
  return (dispatch) => {
    dispatch(settingSocket(socket));
  };
};
