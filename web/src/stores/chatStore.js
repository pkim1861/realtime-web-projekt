import {
  getChatsStart,
  getChatsSuccess,
  getChatsFail,
  updateChatMessagesR,
} from "../reducers/chatReducer";
const axios = require("axios").default;

export const getChats = (userId, channelId) => {
  return (dispatch) => {
    dispatch(getChatsStart());
    axios
      .get(
        `http://localhost:8000/api/users/${encodeURIComponent(
          userId
        )}/channels/${encodeURIComponent(channelId)}/messages`,
        {
          headers: { Authorization: "Bearer " + localStorage.getItem("token") },
        }
      )
      .then((data) => {
        if (data.error !== undefined) {
          dispatch(getChatsFail(data.error));
        } else {
          dispatch(getChatsSuccess(data));
        }
      })
      .catch(() => {
        dispatch(getChatsFail());
      });
  };
};

export const updateChatMessages = (newChatMesssage) => {
  return (dispatch) => {
    dispatch(updateChatMessagesR(newChatMesssage));
  };
};
