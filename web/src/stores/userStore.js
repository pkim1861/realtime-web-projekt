import {
  signUpStart,
  signUpSuccess,
  signUpFail,
  loginStart,
  loginSuccess,
  loginFail,
  logoutStart,
  logoutSuccess,
  logoutFail,
  getAllUsersStart,
  getAllUsersSuccess,
  getAllUsersFail,
} from "../reducers/userReducer";
const axios = require("axios").default;

export const signUp = (user) => {
  return (dispatch) => {
    dispatch(signUpStart());
    let formData = new FormData();
    formData.append("file", user.file);
    formData.append("firstName", user.firstName);
    formData.append("lastName", user.lastName);
    formData.append("email", user.email);
    formData.append("password", user.password);
    axios
      .post("http://localhost:8000/api/users", formData)
      .then((data) => {
        if (data.error !== undefined) {
          dispatch(signUpFail(data.error));
        } else {
          dispatch(signUpSuccess(data));
          alert("Successful signup!");
        }
      })
      .catch((error) => {
        dispatch(signUpFail());
      });
  };
};

export const login = (user) => {
  return (dispatch) => {
    dispatch(loginStart());
    axios
      .post("http://localhost:8000/api/users/login", { user })
      .then((data) => {
        if (data.error !== undefined) {
          dispatch(loginFail(data.error));
        } else {
          dispatch(loginSuccess(data.data));
        }
      })
      .catch((error) => {
        dispatch(loginFail());
      });
  };
};

export const logout = () => {
  return (dispatch) => {
    dispatch(logoutStart());
    axios
      .post(
        "http://localhost:8000/api/users/logout",
        {
          user: localStorage.getItem("user"),
        },
        {
          headers: { Authorization: "Bearer " + localStorage.getItem("token") },
        }
      )
      .then(() => {
        dispatch(logoutSuccess());
      })
      .catch(() => {
        dispatch(logoutFail());
      });
  };
};

export const getAllUsers = () => {
  return (dispatch) => {
    dispatch(getAllUsersStart());
    axios
      .get("http://localhost:8000/api/users/", {
        headers: { Authorization: "Bearer " + localStorage.getItem("token") },
      })
      .then((data) => {
        if (data.error !== undefined) {
          dispatch(getAllUsersFail(data.error));
        } else {
          dispatch(getAllUsersSuccess(data));
        }
      })
      .catch(() => {
        dispatch(getAllUsersFail());
      });
  };
};
