import {
  getProfileDataStart,
  getProfileDataSuccess,
  getProfileDataFail,
} from "../reducers/profileReducer";
const axios = require("axios").default;

export const getProfileData = (id) => {
  return (dispatch) => {
    dispatch(getProfileDataStart());
    axios
      .get(`http://localhost:8000/api/users/${encodeURIComponent(id)}`, {
        headers: { Authorization: "Bearer " + localStorage.getItem("token") },
      })
      .then((data) => {
        if (data.error !== undefined) {
          dispatch(getProfileDataFail(data.error));
        } else {
          dispatch(getProfileDataSuccess(data.data));
        }
      })
      .catch(() => {
        dispatch(getProfileDataFail());
      });
  };
};
