import {
  getCommentsStart,
  getCommentsSuccess,
  getCommentsFail,
  updateCommentSuccess,
} from "../reducers/commentReducer";
const axios = require("axios").default;

export const getComments = (bookId) => {
  return (dispatch) => {
    dispatch(getCommentsStart());
    axios
      .get(
        `http://localhost:8000/api/books/${encodeURIComponent(
          bookId
        )}/comments`,
        {
          headers: { Authorization: "Bearer " + localStorage.getItem("token") },
        }
      )
      .then((data) => {
        if (data.error !== undefined) {
          dispatch(getCommentsFail(data.error));
        } else {
          dispatch(getCommentsSuccess(data));
        }
      })
      .catch(() => {
        dispatch(getCommentsFail());
      });
  };
};

export const updateComments = (newComment) => {
  return (dispatch) => {
    dispatch(updateCommentSuccess(newComment));
  };
};
