import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import UserSlice from "../reducers/userReducer";
import BookSlice from "../reducers/bookReducer";
import ProfileSlice from "../reducers/profileReducer";
import SocketSlice from "../reducers/socketReducer";
import CommentSlice from "../reducers/commentReducer";
import NotificationSlice from "../reducers/notificationReducer";
import ChatSlice from "../reducers/chatReducer";

const reducer = combineReducers({
  user: UserSlice,
  book: BookSlice,
  profile: ProfileSlice,
  socket: SocketSlice,
  comment: CommentSlice,
  notification: NotificationSlice,
  chat: ChatSlice,
});
const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
export default store;
