import React from "react";
import { OverlayTrigger, Popover, Button } from "react-bootstrap";
import { useSelector } from "react-redux";
import { IconContext } from "react-icons";
import { AiFillBell } from "react-icons/ai";

function NotificationComponent(props) {
  const notifications = useSelector(
    (state) => state.notification.notifications
  );

  return (
    <OverlayTrigger
      trigger="focus"
      key={"bottom"}
      placement={"bottom"}
      overlay={
        <Popover id={`popover-positioned-${"bottom"}`}>
          <Popover.Title as="h3">Notifications</Popover.Title>
          <Popover.Content
            style={{ overflow: "auto", height: "70vh", width: "20vw" }}
          >
            {notifications !== null &&
              notifications.map((notification, i) => {
                return <div key={i}>notification.message</div>;
              })}
          </Popover.Content>
        </Popover>
      }
    >
      <Button
        variant="light"
        size="sm"
        onClick={() => {
          return <NotificationComponent />;
        }}
      >
        <IconContext.Provider
          value={{
            color: "#A27C5B",
            className: "global-class-name",
            size: "25px",
            margin: "1000px",
          }}
        >
          <AiFillBell />
        </IconContext.Provider>
      </Button>
    </OverlayTrigger>
  );
}

export default NotificationComponent;
