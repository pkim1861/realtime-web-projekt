import React from "react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getProfileData } from "../../stores/profileStore";
import { Container, Row, Col, Image } from "react-bootstrap";
import { AiOutlineMessage } from "react-icons/ai";
import { IconContext } from "react-icons";
import { Button, OverlayTrigger, Tooltip } from "react-bootstrap";
import BookCard from "../cards/BookCard";
import { getChats } from "../../stores/chatStore";
import ChatModal from "../../modals/ChatModal";
import "../../App.css";

function ProfilePage() {
  const dispatch = useDispatch();
  const { id } = useParams();
  const profileData = useSelector((state) => state.profile.userData);
  const books = useSelector((state) => state.profile.books);
  const userId = useSelector((state) => state.user.user.userId);

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => {
    dispatch(getChats(userId, id));
    setShow(true);
  };

  useEffect(() => {
    dispatch(getProfileData(id));
  }, [dispatch, id]);

  return (
    <Container>
      {profileData !== null && (
        <>
          <Row>
            <Col>
              <Image
                src={`http://localhost:8000/${profileData.profilePicture}`}
                roundedCircle="true"
                style={{
                  width: "15rem",
                }}
              />
            </Col>
            <Col>
              <Row>
                <strong>
                  {profileData.firstName} {profileData.lastName}
                </strong>
              </Row>
              <Row>
                <Col>
                  <OverlayTrigger
                    placement="bottom"
                    delay={{ show: 250, hide: 400 }}
                    overlay={<Tooltip>Send a message</Tooltip>}
                  >
                    <Button variant="light" size="sm" onClick={handleShow}>
                      <IconContext.Provider
                        value={{
                          color: "#A27C5B",
                          className: "global-class-name",
                          size: "25px",
                          margin: "1000px",
                        }}
                      >
                        <AiOutlineMessage />
                      </IconContext.Provider>
                    </Button>
                  </OverlayTrigger>
                </Col>
              </Row>
            </Col>
          </Row>
          <hr />
          <Row>
            <strong>Books uploaded by this user</strong>
          </Row>
          <Row xs={1} md={2} lg={3} xl={4}>
            <BookCard books={books} page="profile" />
          </Row>
        </>
      )}
      <ChatModal
        show={show}
        handleClose={handleClose}
        profileData={profileData}
        id={id}
      />
    </Container>
  );
}

export default ProfilePage;
