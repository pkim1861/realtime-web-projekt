import React from "react";
import BookUploadForm from "../forms/BookUploadForm";
import { Row, Col, Container } from "react-bootstrap";

function SignUpPage() {
  return (
    <Container>
      <Row>
        <h3>Upload a book</h3>
      </Row>
      <Row>
        <Col>
          <BookUploadForm />
        </Col>
      </Row>
    </Container>
  );
}

export default SignUpPage;
