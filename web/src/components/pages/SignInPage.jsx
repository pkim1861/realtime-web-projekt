import React from "react";
import SignInForm from "../forms/SignInForm";
import { Row, Col, Container } from "react-bootstrap";

function SignInPage() {
  return (
    <Container>
      <Row>
        <h3>Sign In</h3>
      </Row>
      <Row>
        <Col>
          <SignInForm />
        </Col>
      </Row>
    </Container>
  );
}

export default SignInPage;
