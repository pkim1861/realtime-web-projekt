import React from "react";
import SignUpForm from "../forms/SignUpForm";
import { Row, Col, Container } from "react-bootstrap";

function SignUpPage() {
  return (
    <Container>
      <Row>
        <h3>Sign Up</h3>
      </Row>
      <Row>
        <Col>
          <SignUpForm />
        </Col>
      </Row>
    </Container>
  );
}

export default SignUpPage;
