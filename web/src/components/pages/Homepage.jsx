import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Row } from "react-bootstrap";
import { getBooks } from "../../stores/bookStore";
import BookCard from "../cards/BookCard";
import "../../App.css";

function Homepage() {
  const dispatch = useDispatch();
  const books = useSelector((state) => state.book.books);

  useEffect(() => {
    dispatch(getBooks());
  }, [dispatch]);

  return (
    <Container>
      {books !== null && books.length !== 0 ? (
        <Row xs={1} md={2} lg={3} xl={4}>
          <BookCard books={books} page="home" />
        </Row>
      ) : (
        <p>No books found</p>
      )}
    </Container>
  );
}

export default Homepage;
