import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import Homepage from "./pages/Homepage";
import SignUpPage from "./pages/SignUpPage";
import SignInPage from "./pages/SignInPage";
import BookUploadPage from "./pages/BookUploadPage";
import ProfilePage from "./pages/ProfilePage";
import { useSelector } from "react-redux";

function Routing() {
  const user = useSelector((state) => state.user.user);

  if (user === null)
    return (
      <Routes>
        <Route exact path="/signUp" element={<SignUpPage />} />
        <Route exact path="/" element={<SignInPage />} />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    );
  else
    return (
      <Routes>
        <Route path="/users/:id" element={<ProfilePage />} />
        <Route exact path="/newBook" element={<BookUploadPage />} />
        <Route exact path="/" element={<Homepage />} />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    );
}

export default Routing;
