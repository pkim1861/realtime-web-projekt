/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Card,
  Modal,
  OverlayTrigger,
  Tooltip,
  Button,
} from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { Image } from "react-bootstrap";
import { IconContext } from "react-icons";
import { MdFavorite, MdFavoriteBorder } from "react-icons/md";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import { setFavorite, setChosenBook } from "../../stores/bookStore";
import Moment from "react-moment";
import CommentsForm from "../forms/CommentsForm";
import { getComments, updateComments } from "../../stores/commentStore";
import { updateNotifications } from "../../stores/notificationStore";

function BookCard(props) {
  const books = props.books;
  const page = props.page;
  let navigate = useNavigate();
  const dispatch = useDispatch();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleChosenBook = (i) => dispatch(setChosenBook(i));

  const socket = useSelector((state) => state.socket.socket);
  const username = useSelector((state) => state.user.user.username);
  const userId = useSelector((state) => state.user.user.userId);
  const comments = useSelector((state) => state.comment.comments);
  const chosenBook = useSelector((state) => books[state.book.selectedBook]);

  for (let i = 0; i < books.length; i++) {
    socket.emit("onFavorites", books[i].id, userId);
    if (books[i].userId === userId) {
      socket.emit("onNotification", books[i].id);
    }
  }
  socket.emit("onNotification", userId);

  socket.on("newFavorite", (newFavorite) => {
    dispatch(
      setFavorite(
        newFavorite.bookId,
        username === newFavorite.username,
        newFavorite.favorite,
        newFavorite.favoriteNr
      )
    );
  });

  socket.on("newComment", (newComment) => {
    dispatch(updateComments(newComment));
  });

  socket.on("newNotification", (newNotification) => {
    dispatch(updateNotifications(newNotification));
  });

  useEffect(() => {
    if (show) dispatch(getComments(chosenBook.id));
  }, [dispatch, chosenBook, show]);

  useEffect(() => {
    if (show) {
      socket.emit("onComments", chosenBook.id, userId);
    }
  }, [show]);

  const markAsFavorite = (fav) => {
    socket.emit("markAsFavorite", {
      bookId: chosenBook.id,
      username,
      fav,
    });
  };

  return books.map((book, i) => (
    <Col key={i}>
      <Card
        style={{
          width: "14.5rem",
          cursor: "pointer",
        }}
        key={i}
      >
        <Card.Img
          variant="top"
          src={`http://localhost:8000/${book.image}`}
          style={{ width: "100%", height: "30vh", objectFit: "contain" }}
          onClick={() => {
            handleChosenBook(i);
            handleShow();
          }}
        />
        <Card.Body>
          <Card.Title>
            <strong>{book.title}</strong>
          </Card.Title>
          <Row>
            <Col>{book.author}</Col>
          </Row>
          <Row>
            <Col>
              <IconContext.Provider
                value={{
                  color: "#A27C5B",
                  className: "global-class-name",
                  size: "25px",
                  margin: "1000px",
                }}
              >
                <MdFavorite />
              </IconContext.Provider>
            </Col>
            <Col>{book.favoriteNr} favorites</Col>
          </Row>
        </Card.Body>
        {page === "home" && (
          <Card.Footer>
            <Row
              as="a"
              onClick={() => {
                let id = book.userId;
                let path = `/users/${encodeURIComponent(id)}`;
                navigate(path);
              }}
            >
              <Image
                src={`http://localhost:8000/${book.userProfilePicture}`}
                roundedCircle="true"
                style={{
                  width: "5rem",
                }}
              />{" "}
              {book.userFirstname + " " + book.userLastname}
            </Row>
          </Card.Footer>
        )}
      </Card>
      {/*<BookModal book={book} show={show} handleClose={handleClose} key={i} />*/}
      {show && (
        <Modal
          show={show}
          onHide={handleClose}
          animation={false}
          className="modal"
          scrollable={true}
          size="lg"
        >
          <Modal.Header closeButton>
            <Modal.Title>
              {chosenBook.title}
              <br />
              {chosenBook.author}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col>
                <Image
                  src={`http://localhost:8000/${chosenBook.image}`}
                  style={{
                    width: "100%",
                    height: "40vh",
                    objectFit: "contain",
                  }}
                />
              </Col>
              <Col>
                <Row
                  as="a"
                  onClick={() => {
                    let id = chosenBook.userId;
                    let path = `/users/${encodeURIComponent(id)}`;
                    navigate(path);
                  }}
                >
                  <Image
                    src={`http://localhost:8000/${chosenBook.userProfilePicture}`}
                    roundedCircle="true"
                    style={{
                      width: "5rem",
                    }}
                  />{" "}
                  {chosenBook.userFirstname + " " + chosenBook.userLastname}
                </Row>
                <Row>
                  <Col>{"Did you like this book? Add to favorites!"}</Col>
                  <Col>
                    <OverlayTrigger
                      placement="bottom"
                      delay={{ show: 250, hide: 400 }}
                      overlay={
                        <Tooltip>
                          {chosenBook.favorite
                            ? "Remove from favorites"
                            : "Add to favorites"}
                        </Tooltip>
                      }
                    >
                      <Button
                        variant="light"
                        onClick={() => {
                          markAsFavorite(!chosenBook.favorite);
                        }}
                      >
                        {chosenBook.favorite ? (
                          <IconContext.Provider
                            value={{
                              color: "#A27C5B",
                              className: "global-class-name",
                              size: "25px",
                              margin: "1000px",
                            }}
                          >
                            <MdFavorite />
                          </IconContext.Provider>
                        ) : (
                          <IconContext.Provider
                            value={{
                              color: "#A27C5B",
                              className: "global-class-name",
                              size: "25px",
                              margin: "1000px",
                            }}
                          >
                            <MdFavoriteBorder />
                          </IconContext.Provider>
                        )}
                      </Button>
                    </OverlayTrigger>
                  </Col>
                </Row>
              </Col>
            </Row>
            <hr />
            <Row>
              <strong>Comments</strong>
              {comments !== null &&
                comments.map((comment, i) => (
                  <Row key={i}>
                    <Col>
                      {comment.commenterFirstName} {comment.commenterLastName}{" "}
                      {":"} {comment.text}
                    </Col>
                    <Col>
                      <Moment format="YYYY/MM/DD HH:mm">
                        {comment.uploadDate}
                      </Moment>
                    </Col>
                  </Row>
                ))}
            </Row>
          </Modal.Body>
          <hr />
          <Modal.Footer class="mr-auto">
            <CommentsForm book={chosenBook} />
            <br />
          </Modal.Footer>
        </Modal>
      )}
    </Col>
  ));
}

BookCard.propTypes = {
  books: PropTypes.array,
  page: PropTypes.string,
};

export default BookCard;
