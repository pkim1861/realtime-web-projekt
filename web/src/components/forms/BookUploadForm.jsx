import React from "react";
import { Button, Row, Col, Form, Container } from "react-bootstrap";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { uploadNewBook } from "../../stores/bookStore";

function SignUpForm() {
  const dispatch = useDispatch();
  const uploadNewBookError = useSelector((state) => state.book.error);
  const user = useSelector((state) => state.user.user);
  const ref = React.useRef();

  function handleClick() {
    ref.current.value = "";
  }

  return (
    <Formik
      initialValues={{
        title: "",
        author: "",
        file: null,
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        dispatch(uploadNewBook(values, user));
        setSubmitting(false);
        handleClick();
        resetForm(true);
      }}
      validationSchema={Yup.object().shape({
        title: Yup.string().required("Title is required"),
        author: Yup.string()
          .matches(
            /^[A-ZÁÉÍÚÜŐŰa-záéúíóüőű\s.,]+$/,
            "The author's name must contain only letters"
          )
          .required("The author's name is required"),
      })}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        setFieldValue,
      }) => (
        <Form onSubmit={handleSubmit}>
          <Container>
            <Row>
              <Form.Control
                type="text"
                name="title"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.title}
                placeholder="Title"
                className="input"
              />
            </Row>
            {errors.title && touched.title && (
              <p style={{ color: "red" }}>{errors.title}</p>
            )}
            <Row>
              <Form.Control
                type="text"
                name="author"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.author}
                placeholder="Author"
                className="input"
              />
            </Row>
            {errors.author && touched.author && (
              <p style={{ color: "red" }}>{errors.author}</p>
            )}

            <Row>
              <Col>
                <Form.Control
                  type="file"
                  name="file"
                  accept="image/*"
                  ref={ref}
                  onChange={(event) =>
                    setFieldValue("file", event.target.files[0])
                  }
                />
              </Col>
            </Row>
            <p style={{ color: "red" }}>{uploadNewBookError}</p>
            <Row>
              <Button
                variant="light"
                size="sm"
                type="submit"
                disabled={isSubmitting}
              >
                Upload
              </Button>
            </Row>
          </Container>
        </Form>
      )}
    </Formik>
  );
}

export default SignUpForm;
