import React from "react";
import { Button, Row, Col, Form, Container } from "react-bootstrap";
import { Formik } from "formik";
import { IconContext } from "react-icons";
import { IoIosSend } from "react-icons/io";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

function ChatForm(props) {
  const socket = useSelector((state) => state.socket.socket);
  const userId = useSelector((state) => state.user.user.userId);

  const onSend = (text) => {
    socket.emit("chat", {
      userId,
      channelId: props.id,
      text,
    });
  };

  return (
    <Formik
      initialValues={{
        message: "",
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        onSend(values.message);
        setSubmitting(false);
        resetForm(true);
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <Form onSubmit={handleSubmit}>
          <Container>
            <Row>
              <Col>
                <Form.Control
                  type="text"
                  name="message"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.message}
                  placeholder="Type a message here"
                  className="input"
                />
              </Col>
              <Col>
                <Button
                  variant="light"
                  size="sm"
                  type="submit"
                  disabled={isSubmitting}
                >
                  <IconContext.Provider
                    value={{
                      color: "#A27C5B",
                      className: "global-class-name",
                      size: "25px",
                      margin: "1000px",
                    }}
                  >
                    <IoIosSend />
                  </IconContext.Provider>
                </Button>
              </Col>
            </Row>
          </Container>
        </Form>
      )}
    </Formik>
  );
}

ChatForm.propTypes = {
  id: PropTypes.string,
};

export default ChatForm;
