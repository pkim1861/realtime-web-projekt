import React from "react";
import { Button, Row, Col, Form, Container } from "react-bootstrap";
import { Formik } from "formik";
import * as Yup from "yup";
import { IconContext } from "react-icons";
import { AiOutlineMail } from "react-icons/ai";
import { RiLockPasswordLine } from "react-icons/ri";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { login } from "../../stores/userStore";

function SignInForm() {
  const dispatch = useDispatch();

  return (
    <Formik
      initialValues={{
        email: "",
        password: "",
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        dispatch(login(values));
        setSubmitting(false);
        resetForm(true);
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string()
          .email("Must be a valid email address")
          .required("Email is required"),
        password: Yup.string()
          .min(5, "Password must contain at least 5 characters")
          .required("Password is required"),
      })}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <Form onSubmit={handleSubmit}>
          <Container>
            <Row>
              <IconContext.Provider
                value={{
                  color: "#A27C5B",
                  className: "global-class-name",
                  size: "25px",
                  margin: "1000px",
                }}
              >
                <AiOutlineMail />
              </IconContext.Provider>
              <Form.Control
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                placeholder="Email"
                className="input"
              />
            </Row>
            {errors.email && touched.email && (
              <p style={{ color: "red" }}>{errors.email}</p>
            )}
            <Row>
              <IconContext.Provider
                value={{
                  color: "#A27C5B",
                  className: "global-class-name",
                  size: "25px",
                  margin: "1000px",
                }}
              >
                <RiLockPasswordLine />
              </IconContext.Provider>
              <Form.Control
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                placeholder="Password"
                className="input"
              />
            </Row>
            {errors.password && touched.password && (
              <p style={{ color: "red" }}>{errors.password}</p>
            )}
            <Row>
              <Button
                variant="light"
                size="sm"
                type="submit"
                disabled={isSubmitting}
              >
                Login
              </Button>
            </Row>
            <Row>
              <Col>
                <label>No account yet?</label>

                <Link to="/signUp">
                  <Button variant="light" size="md">
                    Sign up
                  </Button>
                </Link>
              </Col>
            </Row>
          </Container>
        </Form>
      )}
    </Formik>
  );
}

export default SignInForm;
