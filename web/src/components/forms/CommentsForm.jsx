import React from "react";
import { Button, Row, Col, Form, Container } from "react-bootstrap";
import { Formik } from "formik";
import { IconContext } from "react-icons";
import { IoIosSend } from "react-icons/io";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

function CommentsForm(props) {
  const socket = useSelector((state) => state.socket.socket);
  const username = useSelector((state) => state.user.user.username);

  const onComment = (text) => {
    socket.emit("comment", {
      username,
      channelId: props.book.id,
      bookId: props.book.id,
      text,
    });
  };

  return (
    <Formik
      initialValues={{
        comment: "",
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        onComment(values.comment);
        setSubmitting(false);
        resetForm(true);
      }}
    >
      {({ values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
        <Form onSubmit={handleSubmit}>
          <Container>
            <Row>
              <Col>
                <Form.Control
                  type="text"
                  name="comment"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.comment}
                  placeholder="Type your comment here"
                  className="input"
                />
              </Col>
              <Col>
                <Button
                  variant="light"
                  size="sm"
                  type="submit"
                  disabled={isSubmitting}
                >
                  <IconContext.Provider
                    value={{
                      color: "#A27C5B",
                      className: "global-class-name",
                      size: "25px",
                      margin: "1000px",
                    }}
                  >
                    <IoIosSend />
                  </IconContext.Provider>
                </Button>
              </Col>
            </Row>
          </Container>
        </Form>
      )}
    </Formik>
  );
}

CommentsForm.propTypes = {
  book: PropTypes.object,
};

export default CommentsForm;
