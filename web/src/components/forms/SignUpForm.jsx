import React from "react";
import { Button, Row, Col, Form, Container } from "react-bootstrap";
import { Formik } from "formik";
import * as Yup from "yup";
import { IconContext } from "react-icons";
import { BsPerson } from "react-icons/bs";
import { AiOutlineMail } from "react-icons/ai";
import { CgProfile } from "react-icons/cg";
import { RiLockPasswordLine } from "react-icons/ri";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { signUp } from "../../stores/userStore";

function SignUpForm() {
  const dispatch = useDispatch();
  const signUpError = useSelector((state) => state.user.signUpError);
  const ref = React.useRef();

  function handleClick() {
    ref.current.value = "";
  }

  return (
    <Formik
      initialValues={{
        firstName: "",
        lastName: "",
        email: "",
        file: null,
        password: "",
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        dispatch(signUp(values));
        setSubmitting(false);
        handleClick();
        resetForm(true);
      }}
      validationSchema={Yup.object().shape({
        firstName: Yup.string()
          .matches(
            /^[A-ZÁÉÍÚÜŐŰ][a-záéúíóüőű]+$/,
            "Firstname must contain only letters"
          )
          .required("Firstname is required"),
        lastName: Yup.string()
          .matches(
            /^[A-ZÁÉÍÚÜŐŰ][a-záéúíóüőű]+$/,
            "Lastname must contain only letters"
          )
          .required("Lastname is required"),
        email: Yup.string()
          .email("Must be a valid email address")
          .required("Email is required"),
        password: Yup.string()
          .min(5, "Password must contain at least 5 characters")
          .required("Password is required"),
      })}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        setFieldValue,
      }) => (
        <Form onSubmit={handleSubmit}>
          <Container>
            <Row>
              <IconContext.Provider
                value={{
                  color: "#A27C5B",
                  className: "global-class-name",
                  size: "25px",
                  margin: "1000px",
                }}
              >
                <BsPerson />
              </IconContext.Provider>
              <Form.Control
                type="text"
                name="firstName"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.firstName}
                placeholder="Firstname"
                className="input"
              />
            </Row>
            {errors.firstName && touched.firstName && (
              <p style={{ color: "red" }}>{errors.firstName}</p>
            )}
            <Row>
              <IconContext.Provider
                value={{
                  color: "#A27C5B",
                  className: "global-class-name",
                  size: "25px",
                  margin: "1000px",
                }}
              >
                <BsPerson />
              </IconContext.Provider>
              <Form.Control
                type="text"
                name="lastName"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.lastName}
                placeholder="Lastname"
                className="input"
              />
            </Row>
            {errors.lastName && touched.lastName && (
              <p style={{ color: "red" }}>{errors.lastName}</p>
            )}
            <Row>
              <IconContext.Provider
                value={{
                  color: "#A27C5B",
                  className: "global-class-name",
                  size: "25px",
                  margin: "1000px",
                }}
              >
                <AiOutlineMail />
              </IconContext.Provider>
              <Form.Control
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                placeholder="Email"
                className="input"
              />
            </Row>
            {errors.email && touched.email && (
              <p style={{ color: "red" }}>{errors.email}</p>
            )}
            <Row>
              <IconContext.Provider
                value={{
                  color: "#A27C5B",
                  className: "global-class-name",
                  size: "25px",
                  margin: "1000px",
                }}
              >
                <RiLockPasswordLine />
              </IconContext.Provider>
              <Form.Control
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                placeholder="Password"
                className="input"
              />
            </Row>
            <br />
            <Row>Choose a profile picture</Row>
            <Row>
              <IconContext.Provider
                value={{
                  color: "#A27C5B",
                  className: "global-class-name",
                  size: "25px",
                  margin: "1000px",
                }}
              >
                <CgProfile />
              </IconContext.Provider>
              <Form.Control
                type="file"
                name="file"
                accept="image/*"
                ref={ref}
                onChange={(event) =>
                  setFieldValue("file", event.target.files[0])
                }
              />
            </Row>
            {errors.password && touched.password && (
              <p style={{ color: "red" }}>{errors.password}</p>
            )}

            <p style={{ color: "red" }}>{signUpError}</p>

            <Row>
              <Button
                variant="light"
                size="sm"
                type="submit"
                disabled={isSubmitting}
              >
                Sign up
              </Button>
            </Row>
            <Row>
              <Col>
                <label>Already have an account?</label>

                <Link to="/">
                  <Button variant="light" size="md">
                    Login
                  </Button>
                </Link>
              </Col>
            </Row>
          </Container>
        </Form>
      )}
    </Formik>
  );
}

export default SignUpForm;
