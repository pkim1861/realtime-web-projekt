import React, { useEffect, useState } from "react";
import { Navbar, Button, Nav, Badge } from "react-bootstrap";
import { Link } from "react-router-dom";
import { IconContext } from "react-icons";
import { AiFillHome, AiFillBell } from "react-icons/ai";
import { useSelector, useDispatch } from "react-redux";
import { logout, getAllUsers } from "../../stores/userStore";
import {
  getNotifications,
  setNotificationsSeen,
  setNotificationsLocallySeen,
} from "../../stores/notificationStore";
import NotificationModal from "../../modals/NotificationModal";
import UsersModal from "../../modals/UsersModal";

function NavBar() {
  const dispatch = useDispatch();

  const user = useSelector((state) => state.user.user);
  const userId = useSelector((state) =>
    state.user.user === null ? null : state.user.user.userId
  );
  /*const notifications = useSelector(
    (state) => state.notification.notifications
  );*/

  const notSeenNotificationNr = useSelector(
    (state) => state.notification.notSeenNotificationNr
  );

  // notification modal
  const [show, setShow] = useState(false);

  const handleClose = () => {
    dispatch(setNotificationsLocallySeen());
    setShow(false);
  };
  const handleShow = () => {
    dispatch(setNotificationsSeen(userId));
    setShow(true);
  };

  // users modal
  const [showUsers, setShowUsers] = useState(false);

  const handleCloseUsers = () => {
    setShowUsers(false);
  };
  const handleShowUsers = () => {
    dispatch(getAllUsers(userId));
    setShowUsers(true);
  };

  useEffect(() => {
    if (user !== null) {
      dispatch(getNotifications(userId));
    }
  }, [user, userId, dispatch]);

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Navbar.Brand as={Link} to="/">
          <IconContext.Provider
            value={{
              color: "#A27C5B",
              className: "global-class-name",
              size: "25px",
              margin: "1000px",
            }}
          >
            <AiFillHome />
          </IconContext.Provider>
        </Navbar.Brand>
        {user === null ? (
          <>
            <Nav.Link as={Link} to="/signUp">
              <Button variant="light" size="sm">
                Sign up
              </Button>
            </Nav.Link>
            <Nav.Link as={Link} to="/">
              <Button variant="light" size="sm">
                Login
              </Button>
            </Nav.Link>
          </>
        ) : (
          <>
            <Nav.Link as={Link} to="/newBook">
              <Button variant="light" size="sm">
                New book
              </Button>
            </Nav.Link>
            <Nav>
              <Button
                variant="light"
                size="sm"
                onClick={() => handleShowUsers()}
              >
                Users
              </Button>
            </Nav>
            <Nav>
              <Button
                variant="light"
                size="sm"
                onClick={() => dispatch(logout())}
              >
                Logout
              </Button>
            </Nav>
            <Nav>
              <Button
                variant="light"
                size="sm"
                onClick={() => {
                  handleShow();
                }}
              >
                <IconContext.Provider
                  value={{
                    color: "#A27C5B",
                    className: "global-class-name",
                    size: "25px",
                    margin: "1000px",
                  }}
                >
                  <AiFillBell />
                </IconContext.Provider>
                {notSeenNotificationNr > 0 && (
                  <Badge pill bg="danger">
                    {notSeenNotificationNr}
                  </Badge>
                )}
              </Button>
            </Nav>
            <NotificationModal show={show} handleClose={handleClose} />
            <UsersModal show={showUsers} handleClose={handleCloseUsers} />
          </>
        )}
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavBar;
