const amqp = require('amqplib');

module.exports.initializeAmqpChannel = () => amqp.connect({
  hostname: process.env.RABBITMQ_HOST || 'localhost',
  port: Number(process.env.RABBITMQ_PORT) || 5672,
  username: process.env.RABBITMQ_USER || 'guest',
  password: process.env.RABBITMQ_PASSWORD || 'guest',
})
  .then((connection) => connection.createChannel())
  .then((channel) => {
    channel.assertExchange('message', 'topic', { durable: true });
    return channel;
  });
