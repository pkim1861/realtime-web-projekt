const io = require('socket.io')();
const commentService = require('../services/commentService');
const notificationService = require('../services/notificationService');
const bookService = require('../services/bookService');
const amqpConfig = require('../config/amqpConfig');
const chatService = require('../services/chatService');
const socketMappers = require('../mappers/socketMappers');
const logger = require('../config/loggerConfig');

const socketapi = {
  io,
};

const extendedSocket = socketMappers.extendedSocket();

const removeConsumerrTags = ((channel, tagName, id) => new Promise((resolve, reject) => {
  if (extendedSocket[tagName].length > 0) {
    const promises = [];
    const newList = [];
    for (let i = 0; i < extendedSocket[tagName].length; i += 1) {
      if (id === extendedSocket[tagName][i].id) {
        promises.push(new Promise((resolve2, reject2) => {
          channel.cancel(extendedSocket[tagName][i].tag)
            .then(() => { resolve2(); })
            .catch(() => { reject2(); });
        }));
      } else {
        newList.push(extendedSocket[tagName][i]);
      }
    }
    extendedSocket[tagName] = newList;
    Promise.all(promises)
      .then(() => {
        resolve();
      })
      .catch((er) => {
        logger.level = 'error';
        logger.error('socket: delete consumers', er);
        reject();
      });
  } else {
    resolve();
  }
}));

amqpConfig.initializeAmqpChannel()
  .then((channel) => {
    io.on('connection', (socket) => {
      extendedSocket.socket = socket;
      socket.on('comment', (comment) => {
        const id = `${comment.bookId.substring(1).split(':')[0]}.${comment.bookId.split(':')[1]}`;
        commentService.insertNewComment(comment.text, comment.username, comment.bookId)
          .then((commentInserted) => {
            const returnComment = commentInserted;
            returnComment.type = 'comment';
            let payload = Buffer.from(
              JSON.stringify(returnComment),
            );
            channel.publish('message', `channel.comment.${id}`, payload);

            // notification part

            if (commentInserted.recieverUser !== comment.username) {
              notificationService.insertNewNotification(`${comment.username} commented on your book`, comment.bookId)
                .then((notification) => {
                  payload = Buffer.from(
                    JSON.stringify(notification),
                  );

                  channel.publish('message', `channel.notification.${id}`, payload);
                })
                .catch((error) => {
                  logger.level = 'error';
                  logger.error('socket: insert notification', error);
                });
            }
          });
      });
      socket.on('onComments', (channelId, userId) => {
        const id = `${channelId.substring(1).split(':')[0]}.${channelId.split(':')[1]}`;
        const id2 = `${id}.${userId.substring(1).split(':')[0]}.${userId.split(':')[1]}`;
        removeConsumerrTags(channel, 'commentConsumerTag', id2)
          .then(() => {
            const { queue } = channel.assertQueue('', { exclusive: true, autoDelete: true });
            channel.bindQueue(queue, 'message', `channel.comment.${id}`);
            channel.consume(queue, (payload) => {
              const message = JSON.parse(payload.content.toString());
              if (message.type === 'comment') { socket.emit('newComment', message); }
              channel.ack(payload);
            })
              .then((consumerTag) => {
                extendedSocket.commentConsumerTag.push({ id2, tag: consumerTag.consumerTag });
              });
          })
          .catch((error) => {
            logger.level = 'error';
            logger.error('socket: delete consumers', error);
          });
      });

      socket.on('onCloseComments', () => {
        if (extendedSocket.consumerTag) {
          channel.cancel(extendedSocket.consumerTag);
        }
      });
      socket.on('markAsFavorite', (message) => {
        const id = `${message.bookId.substring(1).split(':')[0]}.${message.bookId.split(':')[1]}`;
        bookService.markFavorite(message.username, message.bookId, message.fav)
          .then((book) => {
            let payload = Buffer.from(
              JSON.stringify({
                type: 'favorite', bookId: message.bookId, favorite: book.favorite, favoriteNr: book.favoriteNr, username: message.username,
              }),
            );
            channel.publish('message', `channel.favorite.${id}`, payload);

            // notification part
            // ne kapjon ertesitest ha o csinalja
            // csak akkor kap ertesitest ha valaki hozzaadja a kedvencekhez, ha eltavolitja nem
            if (message.fav && book.username !== message.username) {
              notificationService.insertNewNotification(`${message.username} added your book to favorites`, message.bookId)
                .then((notification) => {
                  payload = Buffer.from(
                    JSON.stringify(notification),
                  );

                  channel.publish('message', `channel.notification.${id}`, payload);
                })
                .catch((error) => {
                  logger.level = 'error';
                  logger.error('socket: insert notification', error);
                });
            }
          });
      });
      socket.on('onFavorites', (channelId, userId) => {
        const id = `${channelId.substring(1).split(':')[0]}.${channelId.split(':')[1]}`;
        const id2 = `${id}.${userId.substring(1).split(':')[0]}.${userId.split(':')[1]}`;
        removeConsumerrTags(channel, 'favoriteConsumerTag', id2)
          .then(() => {
            const { queue } = channel.assertQueue('', { exclusive: true, autoDelete: true });
            channel.bindQueue(queue, 'message', `channel.favorite.${id}`);
            channel.consume(queue, (payload) => {
              const message = JSON.parse(payload.content.toString());
              if (message.type === 'favorite') {
                socket.emit('newFavorite', message);
              }
              channel.ack(payload);
            })
              .then((consumerTag) => {
                extendedSocket.favoriteConsumerTag.push({ id2, tag: consumerTag.consumerTag });
              })
              .catch((e) => {
                logger.level = 'error';
                logger.error('socket: can not consume favorite', e);
              });
          })
          .catch((error) => {
            logger.level = 'error';
            logger.error('socket: can not remove consumers', error);
          });
      });

      // notification
      socket.on('onNotification', (channelId) => {
        const id = `${channelId.substring(1).split(':')[0]}.${channelId.split(':')[1]}`;
        removeConsumerrTags(channel, 'notificationConsumerTag', id)
          .then(() => {
            const { queue } = channel.assertQueue('', { exclusive: true, autoDelete: true });
            channel.bindQueue(queue, 'message', `channel.notification.${id}`);
            channel.consume(queue, (payload) => {
              const message = JSON.parse(payload.content.toString());
              socket.emit('newNotification', message);
              channel.ack(payload);
            })
              .then((consumerTag) => {
                extendedSocket.notificationConsumerTag.push({ id, tag: consumerTag.consumerTag });
              })
              .catch((error) => {
                logger.level = 'error';
                logger.error('socket: can not consume notification', error);
              });
          }).catch((error) => {
            logger.level = 'error';
            logger.error('socket: can not consume notification', error);
          });
      });

      // chat
      socket.on('chat', (chat) => {
        const id1 = `${chat.channelId.substring(1).split(':')[0]}.${chat.channelId.split(':')[1]}`;
        const id2 = `${chat.userId.substring(1).split(':')[0]}.${chat.userId.split(':')[1]}`;

        chatService.insertChatMessage(chat.userId, chat.channelId, chat.text)
          .then((commentInserted) => {
            const returnChatMessage = commentInserted;
            returnChatMessage.type = 'chat';
            let payload = Buffer.from(
              JSON.stringify(returnChatMessage),
            );
            channel.publish('message', `channel.chat.${id1}`, payload);
            channel.publish('message', `channel.chat.${id2}`, payload);

            // notification part
            notificationService.insertNewChatNotification(`${returnChatMessage.userFromUsername} sent you a message`, chat.channelId)
              .then((notification) => {
                payload = Buffer.from(
                  JSON.stringify(notification),
                );

                channel.publish('message', `channel.notification.${id1}`, payload);
              })
              .catch((error) => {
                logger.level = 'error';
                logger.error('socket: error inserting notification', error);
              });
          });
      });

      socket.on('onChats', (channelId) => {
        const id = `${channelId.substring(1).split(':')[0]}.${channelId.split(':')[1]}`;
        removeConsumerrTags(channel, 'chatConsumerTag', id)
          .then(() => {
            const { queue } = channel.assertQueue('', { exclusive: true, autoDelete: true });
            channel.bindQueue(queue, 'message', `channel.chat.${id}`);
            channel.consume(queue, (payload) => {
              const message = JSON.parse(payload.content.toString());
              if (message.type === 'chat') { socket.emit('newChat', message); }
              channel.ack(payload);
            })
              .then((consumerTag) => {
                extendedSocket.chatConsumerTag.push({ id, tag: consumerTag.consumerTag });
              })
              .catch((error) => {
                logger.level = 'error';
                logger.error('socket: can not consume', error);
              });
          }).catch((error) => {
            logger.level = 'error';
            logger.error('socket: delete consumers', error);
          });
      });
    });

    logger.level = 'info';
    logger.info('socket: A user connected');
  });

module.exports = socketapi;
