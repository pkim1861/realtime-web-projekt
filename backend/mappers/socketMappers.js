module.exports.extendedSocket = (() => ({
  socket: null,
  commentConsumerTag: [],
  chatConsumerTag: [],
  notificationConsumerTag: [],
  favoriteConsumerTag: [],
}));
