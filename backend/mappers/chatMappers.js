const toChatDto = ((dbChat) => ({
  id: `#${Object.values(dbChat)[1].cluster}:${Object.values(dbChat)[1].position}`,
  message: dbChat.message,
  userFrom: dbChat.userFrom,
  userFromFirstName: dbChat.userFromFirstName,
  userFromLastName: dbChat.userFromLastName,
  userFromUsername: dbChat.userFromUsername,
  userTo: dbChat.userTo,
  userToFirstName: dbChat.userToFirstName,
  userToLastName: dbChat.userToLastName,
  date: dbChat.date,
}));

module.exports.toChatDto = toChatDto;
