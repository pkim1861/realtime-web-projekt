const toCommentShortDto = ((dbComment) => ({
  id: `#${Object.values(dbComment)[4].cluster}:${Object.values(dbComment)[4].position}`,
  text: dbComment.text,
}));

const toCommentLongDto = ((dbComment) => ({
  id: `#${Object.values(dbComment)[1].cluster}:${Object.values(dbComment)[1].position}`,
  text: dbComment.text,
  uploadDate: dbComment.uploadDate,
  commenterFirstName: dbComment.firstName,
  commenterLastName: dbComment.lastName,
  commenterImage: dbComment.image,
}));

const toCommentAfterInsertDto = ((dbComment) => ({
  id: `#${Object.values(dbComment)[1].cluster}:${Object.values(dbComment)[1].position}`,
  text: dbComment.text,
  uploadDate: dbComment.uploadDate,
  commenterFirstName: dbComment.firstName,
  commenterLastName: dbComment.lastName,
  commenterImage: dbComment.image,
  recieverUser: dbComment.recieverUser,
}));

module.exports.toCommentShortDto = toCommentShortDto;
module.exports.toCommentLongDto = toCommentLongDto;
module.exports.toCommentAfterInsertDto = toCommentAfterInsertDto;
