const toBookInsertDto = ((dbBook) => ({
  id: `#${Object.values(dbBook)[6].cluster}:${Object.values(dbBook)[6].position}`,
  author: dbBook.author,
  title: dbBook.title,
  image: dbBook.image,
  uploadDate: dbBook.uploadDate,
}));

const toBookGetDto = ((dbBook) => ({
  id: `#${Object.values(dbBook)[1].cluster}:${Object.values(dbBook)[1].position}`,
  author: dbBook.author,
  title: dbBook.title,
  image: dbBook.image,
  uploadDate: dbBook.uploadDate,
  userFirstname: dbBook.userFirstname,
  userLastname: dbBook.userLastname,
  userId: dbBook.userId,
  userProfilePicture: dbBook.userProfilePicture,
  favorite: dbBook.favorite,
  favoriteNr: dbBook.favoriteNr,
}));

const toSocketDto = ((dbBook) => ({
  favorite: dbBook.favorite,
  favoriteNr: dbBook.favoriteNr,
  username: dbBook.username,
}));

const toBookGetShortDto = ((dbBook) => ({
  id: `#${Object.values(dbBook)[1].cluster}:${Object.values(dbBook)[1].position}`,
  author: dbBook.author,
  title: dbBook.title,
  image: dbBook.image,
  uploadDate: dbBook.uploadDate,
}));

module.exports.toBookInsertDto = toBookInsertDto;
module.exports.toBookGetDto = toBookGetDto;
module.exports.toBookGetShortDto = toBookGetShortDto;
module.exports.toSocketDto = toSocketDto;
