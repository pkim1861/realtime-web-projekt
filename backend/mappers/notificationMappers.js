const toNotificationDto = ((dbNotification) => ({
  id: `#${Object.values(dbNotification)[1].cluster}:${Object.values(dbNotification)[1].position}`,
  message: dbNotification.message,
  date: dbNotification.date,
  seen: dbNotification.seen,
}));

const toInsertNotificationDto = ((dbNotification) => ({
  id: `#${Object.values(dbNotification)[6].cluster}:${Object.values(dbNotification)[6].position}`,
  message: dbNotification.message,
  date: dbNotification.date,
  seen: dbNotification.seen,
}));

module.exports.toNotificationDto = toNotificationDto;
module.exports.toInsertNotificationDto = toInsertNotificationDto;
