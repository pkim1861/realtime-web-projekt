const toUserSignUpDto = ((dbUser) => ({
  id: `#${Object.values(dbUser)[7].cluster}:${Object.values(dbUser)[7].position}`,
  firstName: dbUser.firstName,
  lastName: dbUser.lastName,
  username: dbUser.username,
  image: dbUser.image,
}));

const toGetUserDto = ((dbUser) => ({
  id: `#${Object.values(dbUser)[1].cluster}:${Object.values(dbUser)[1].position}`,
  firstName: dbUser.firstName,
  lastName: dbUser.lastName,
  username: dbUser.username,
  image: dbUser.image,
}));

const toGetUserWithBooksDto = ((dbUser, books) => ({
  id: `#${Object.values(dbUser)[1].cluster}:${Object.values(dbUser)[1].position}`,
  firstName: dbUser.firstName,
  lastName: dbUser.lastName,
  username: dbUser.username,
  image: dbUser.image,
  books,
}));

module.exports.toUserSignUpDto = toUserSignUpDto;
module.exports.toGetUserDto = toGetUserDto;
module.exports.toGetUserWithBooksDto = toGetUserWithBooksDto;
