const commentDb = require('../db/commentDb');
const logger = require('../config/loggerConfig');
const commentMappers = require('../mappers/commentMappers');

// uj komment beszurasa
module.exports.insertNewComment = ((text, username, bookId) => new Promise((resolve, reject) => {
  // szukseges informaciok megletenek ellenorzese
  if (!text || !username || !bookId) {
    logger.level = 'error';
    logger.error('commentService: insertNewComment: Not enough information provided');
    reject(new Error('Not enough information provided'));
  } else {
    const uploadDate = new Date();
    // beszuras az adatbazisba
    commentDb.insertComment(text, uploadDate)
      .then((dbComment) => {
        const comment = commentMappers.toCommentShortDto(dbComment);
        // letrehozunk egy elete a beszurt komment es a felhasznalo kozott aki ezt a kommentet irta
        commentDb.createCommentLeavedBbyUser(username, comment.id)
          .then(() => {
            commentDb.createCommentToBook(comment.id, bookId)
              .then(() => {
                commentDb.getCommentById(comment.id)
                  .then((commentInserted) => {
                    resolve(commentMappers.toCommentAfterInsertDto(commentInserted[0]));
                  });
              });
          })
          .catch((e) => {
            logger.level = 'error';
            logger.error('commentService: insertNewComment: Database error', e);
            reject(new Error('Database error'));
          });
      })
      .catch((e) => {
        logger.level = 'error';
        logger.error('commentCervice: insertNewComment: Database error', e);
        reject(new Error('Database error'));
      });
  }
}));

// a kommentek egy konyv szamara
module.exports.getCommentsByBook = ((bookId) => new Promise((resolve, reject) => {
  commentDb.getComments(bookId)
    .then((dbComments) => {
      const comments = [];
      dbComments.forEach((dbComment) => {
        comments.push(commentMappers.toCommentLongDto(dbComment));
      });
      resolve(comments);
    })
    .catch((error) => {
      logger.level = 'error';
      logger.error('commentService: getCommentsByBook: Database error', error);
      reject(new Error('Database error'));
    });
}));
