const validator = require('validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const userDb = require('../db/userDb');
const bookDb = require('../db/bookDb');
const logger = require('../config/loggerConfig');
const userMappers = require('../mappers/userMappers');
const bookMappers = require('../mappers/bookMappers');

const saltRounds = 10;

// uj felhasznalo regisztralasa
module.exports.signUpNewUser = ((userData, file) => new Promise((resolve, reject) => {
  const {
    firstName, lastName, email, password,
  } = userData;

  let path = 'uploads/pic.jpg';
  try {
    path = file.path;
  } catch (e) {
    path = 'uploads/pic.jpg';
  }

  // szukseges informaciok meglete
  if (!email || !firstName || !lastName || !password) {
    logger.level = 'error';
    logger.error('userService: signUpNewUser: All the fields are required');
    reject(new Error('All the fields are required'));
  } else
  // informaciok helyessegenek leellenorzese
  if (!validator.isLength(email, { min: 5 }) || !validator.isLength(password, { min: 5 })) {
    logger.level = 'error';
    logger.error('userService: sigUpNewUser: Not correct signup data');
    reject(new Error('Not correct signup data'));
  } else
  if (!validator.isEmail(email)) {
    logger.level = 'error';
    logger.error('userService: signUpNewUser: Username must be an email address');
    reject(new Error('All the fields are required'));
  } else {
    // megnezi hogy az adott felhasznalonevvel van e mar felhasznalo
    userDb.selectByUsername(email)
      .then((user) => {
        if (user.length !== 0) {
          logger.level = 'error';
          logger.error('userService: signUpNewUser: The username is already taken');
          reject(new Error('The username is already taken'));
        } else {
          // hash and salt password
          bcrypt.genSalt(saltRounds, (err, salt) => {
            if (err) {
              logger.level = 'error';
              logger.error('userService: signUpNewUser: ', err);
              reject(new Error('error'));
            } else {
              bcrypt.hash(password, salt, (hashErr, hash) => {
                if (hashErr) {
                  logger.level = 'error';
                  logger.error('userService: signUpNewUser: ', hashErr);
                  reject(new Error('error'));
                } else {
                  // beszuras az adatbazisba
                  userDb.insertUser(firstName, lastName, email, path, hash)
                    .then((u) => {
                      resolve(userMappers.toUserSignUpDto(u));
                    }).catch((error) => {
                      logger.level = 'error';
                      logger.error('userService: signUpNewUser: ', error);
                      reject(new Error('error'));
                    });
                }
              });
            }
          });
        }
      });
  }
}));

// bejelentkezes
module.exports.login = ((userData) => new Promise((resolve, reject) => {
  const {
    email,
  } = userData;

  const providedPassword = userData.password;

  // szukseges informaciok megletenek ellenorzese
  if (!email || !providedPassword) {
    logger.level = 'error';
    logger.error('userService: login: All the fields are required');
    reject(new Error('All the fields are required'));
  } else
  // helyesek-e a megadott informaciok
  if (!validator.isLength(email, { min: 5 }) || !validator.isLength(providedPassword, { min: 5 })) {
    logger.level = 'error';
    logger.error('userService: login: Not correct signup data');
    reject(new Error('Not correct signup data'));
  } else
  if (!validator.isEmail(email)) {
    logger.level = 'error';
    logger.error('userService: login: Username must be an email address');
    reject(new Error('All the fields are required'));
  } else {
    // lekerjuk az adatbazisbol a jelszot a felhasznalonev alapjan
    userDb.selectUserNameAndPassword(email)
      .then((user) => {
        if (user.length === 0) {
          logger.level = 'error';
          logger.error('userService: login: No user with that email address');
          reject(new Error('The username is already taken'));
        } else {
          // osszehasonlitjuk a megadott jelszo hashelt valtozatat az adatbazisba lementettel
          bcrypt.compare(providedPassword, user[0].password, (err, result) => {
            if (err) {
              logger.level = 'error';
              logger.error('userService: login: ', err);
              reject(new Error('Incorrect password'));
            } else if (result) {
            // log in
            // jwt
              const payload = { email };
              const token = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
                algorithm: 'HS256',
                expiresIn: '1d',
              });

              resolve({ token, email, userId: user[0].id });
            }
          });
        }
      });
  }
}));

// minden felhasznalo lekerese
module.exports.getAllUsers = (() => new Promise((resolve, reject) => {
  userDb.selectAll()
    .then((dbUsers) => {
      const users = [];
      dbUsers.forEach((dbUser) => {
        users.push(userMappers.toGetUserDto(dbUser));
      });
      resolve(users);
    })
    .catch((error) => {
      logger.level = 'error';
      logger.error('userService: getAllUsers: error', error);
      reject(new Error('Database error'));
    });
}));

// felhasznalo lekerese felhasznalonev alapjan
module.exports.getByUsername = ((username) => new Promise((resolve, reject) => {
  userDb.selectByUsername(username)
    .then((dbUser) => {
      if (dbUser.length === 0) {
        logger.level = 'error';
        logger.error('userService: getByUsername: No user with that username');
        reject(new Error('No user with that username'));
      } else {
        resolve(userMappers.toGetUserDto(dbUser));
      }
    })
    .catch((error) => {
      logger.level = 'error';
      logger.error('userService: getByUsername: error', error);
      reject(new Error('Database error'));
    });
}));

// felhasznalo lekerese ID alapjan
module.exports.getById = ((id, username) => new Promise((resolve, reject) => {
  userDb.selectById(id)
    .then((dbUser) => {
      if (dbUser.length === 0) {
        logger.level = 'error';
        logger.error('userService: getById: No user with that username');
        reject(new Error('No user with that id'));
      } else {
        // lekerjuk a felhasznalohoz tartozo konyveket is
        bookDb.getBookByUserId(id, username)
          .then((dbBooks) => {
            const books = [];
            dbBooks.forEach((element) => {
              books.push(bookMappers.toBookGetDto(element));
            });
            resolve(userMappers.toGetUserWithBooksDto(dbUser[0], books));
          })
          .catch((error) => {
            logger.level = 'error';
            logger.error('userService: getById: error', error);
            reject(new Error('Database error'));
          });
      }
    })
    .catch((error) => {
      logger.level = 'error';
      logger.error('userService: getById: error', error);
      reject(new Error('Database error'));
    });
}));
