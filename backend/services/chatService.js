const chatDb = require('../db/chatDb');
const userDb = require('../db/userDb');
const logger = require('../config/loggerConfig');
const chatMappers = require('../mappers/chatMappers');

// uj chat uzenet beszurasa
module.exports.insertChatMessage = ((userIdFrom, userIdTo, message) => new Promise(
  (resolve, reject) => {
    // ellenorizzuk, hogy jelen vannak a szukseges informaciok
    if (!userIdFrom || !userIdTo || !message) {
      logger.level = 'error';
      logger.error('chatService: insertChatMessage: Not enough information provided');
      reject(new Error('Not enough information provided'));
    } else {
      const uploadDate = new Date();
      // beszurjuk az adatbazisba
      chatDb.insertChatMessage(uploadDate, message, userIdFrom, userIdTo)
        .then((dbInsertChat) => {
          const chatId = `#${Object.values(dbInsertChat)[6].cluster}:${Object.values(dbInsertChat)[6].position}`;
          chatDb.getChatsById(chatId)
            .then((dbChat) => {
              resolve(chatMappers.toChatDto(dbChat[0]));
            })
            .catch(() => {
              logger.level = 'error';
              logger.error('chatService: insertChatMessage: Error');
              reject(new Error('InsertChatMessage error'));
            });
        })
        .catch((e) => {
          logger.level = 'error';
          logger.error('commentCervice: insertNewComment: Database error', e);
          reject(new Error('Database error'));
        });
    }
  },
));

// lekeri ket felhasznalo kozti uzeneteket
module.exports.getChatMessages = ((userId, channelId) => new Promise((resolve, reject) => {
  // leteznek e felhasznalok a megadott ID-val
  userDb.selectById(userId)
    .then(() => {
      userDb.selectById(channelId)
        .then(() => {
          chatDb.getChatsByUser(userId, channelId)
            .then((dbMessages) => {
              const messages = [];
              dbMessages.forEach((dbMessage) => {
                messages.push(chatMappers.toChatDto(dbMessage));
              });
              resolve(messages);
            })
            .catch(() => {
              logger.level = 'error';
              logger.error('chatService: getChatMessages: Database error');
              reject(new Error('chatService: getChatMessages: Database error'));
            });
        })
        .catch((error) => {
          logger.level = 'error';
          logger.error('chatService: getChatMessages: ', error);
          reject(new Error('User with the given id does not exist'));
        });
    })
    .catch((error) => {
      logger.level = 'error';
      logger.error('chatService: getChatMessages: ', error);
      reject(new Error('User with the given id does not exist'));
    });
}));
