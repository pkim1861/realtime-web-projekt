const bookDb = require('../db/bookDb');
const userDb = require('../db/userDb');
const logger = require('../config/loggerConfig');
const bookMappers = require('../mappers/bookMappers');

// egy uj konyv feltoltese
module.exports.uploadNewBook = ((data, file) => new Promise((resolve, reject) => {
  const { title, author, username } = data;
  let path = 'uploads/pic.jpg';
  try {
    path = file.path;
  } catch (e) {
    path = 'uploads/pic.jpg';
  }

  // szukseges informaciok megletenek ellenorzese
  if (!title || !author) {
    logger.level = 'error';
    logger.error('bookService: uploadNewBook: Not enough information provided');
    reject(new Error('Not enough information provided'));
  } else {
    const uploadDate = new Date();
    // konyv beszurasa az adatbazisba
    bookDb.insertBook(title, author, uploadDate, path)
      .then((b) => {
        const book = bookMappers.toBookInsertDto(b);
        // szukseges el letrehozasa a konyv es a felhasznalo kozott
        bookDb.createBookUploadedByUser(username, book.id)
          .then(() => {
            resolve(book);
          })
          .catch((e) => {
            logger.level = 'error';
            logger.error('bookService: uploadNewBook: Database error', e);
            reject(new Error('Database error'));
          });
      })
      .catch((e) => {
        logger.level = 'error';
        logger.error('bookService: uploadNewBook: Database error', e);
        reject(new Error('Database error'));
      });
  }
}));

// lekeri az osszes konyvet
module.exports.getBooks = ((username) => new Promise((resolve, reject) => {
  bookDb.getBooks(username)
    .then((dbBooks) => {
      const books = [];
      dbBooks.forEach((element) => {
        books.push(bookMappers.toBookGetDto(element));
      });
      resolve(books);
    })
    .catch((e) => {
      logger.level = 'error';
      logger.error('bookService: getBooks:', e);
      reject(new Error('Database error'));
    });
}));

// kedvencekehz adhat egy konyvet a felhasznalo
module.exports.markFavorite = ((username, bookId, fav) => new Promise((resolve, reject) => {
  // eloszor ellenorizzuk hogy letezik-e a felhasznalo
  userDb.selectByUsername(username)
    .then((user) => {
      if (user.length === 0) {
        logger.level = 'error';
        logger.error('producerService: markFavorite: User does not exist');
        reject(new Error('User does not exist'));
      } else
      // kedvencekhez adas vagy eltavolitas a kedvencek kozul
      if (fav) {
        // letrehozza a szukseges elet
        bookDb.createFavorite(username, bookId)
          .then(() => {
            bookDb.getBookById(bookId, username)
              .then((dbBook) => {
                resolve(bookMappers.toSocketDto(dbBook[0]));
              })
              .catch((error) => {
                logger.level = 'error';
                logger.error('bookService: markFavorite: ', error);
                reject(new Error('Database error'));
              });
          }).catch((error) => {
            logger.level = 'error';
            logger.error('bookService: markFavorite: ', error);
            reject(new Error('Database error'));
          });
      } else {
        // toroljuk a szukseges elet
        bookDb.dropFavorite(username, bookId)
          .then(() => {
            bookDb.getBookById(bookId, username)
              .then((dbBook) => {
                resolve(bookMappers.toSocketDto(dbBook[0]));
              })
              .catch((error) => {
                logger.level = 'error';
                logger.error('bookService: markFavorite: ', error);
                reject(new Error('Database error'));
              });
          }).catch((error) => {
            logger.level = 'error';
            logger.error('bookService: markFavorite: ', error);
            reject(new Error('Database error'));
          });
      }
    }).catch((error) => {
      logger.level = 'error';
      logger.error('producerService: markFavorite: ', error);
      reject(new Error('Database error'));
    });
}));
