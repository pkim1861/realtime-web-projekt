const notificationDb = require('../db/notificationDb');
const bookDb = require('../db/bookDb');
const logger = require('../config/loggerConfig');
const notificationMappers = require('../mappers/notificationMappers');

// uj ertesites beszurasa
module.exports.insertNewNotification = ((message, bookId) => new Promise((resolve, reject) => {
  // szukseges informaciok meglete
  if (!message) {
    logger.level = 'error';
    logger.error('notificationService: insertNewNotification: Not enough information provided');
    reject(new Error('Not enough information provided'));
  } else {
    // megkeresi melyik user toltotte fel az adott kinyvet, hogy o kapjon ertesitest
    bookDb.selectUserByBook(bookId)
      .then((user) => {
        const uploadDate = new Date();
        const userId = `#${Object.values(user)[0].userId[0].cluster}:${Object.values(user)[0].userId[0].position}`;
        // beszurja az ertesitest az adatbazisba
        notificationDb.insertNotification(`${message} (${user[0].author}: ${user[0].title})`, uploadDate, userId)
          .then((dbNotification) => {
            const notification = notificationMappers.toInsertNotificationDto(dbNotification);

            resolve(notification);
          })
          .catch((e) => {
            logger.level = 'error';
            logger.error('notificationService: insertNewNotification: Database error', e);
            reject(new Error('Database error'));
          });
      });
  }
}));

// chat ertesites beszurasa
module.exports.insertNewChatNotification = ((message, userId) => new Promise((resolve, reject) => {
  // szukseges informaciok meglete
  if (!message || !userId) {
    logger.level = 'error';
    logger.error('notificationService: insertNewNotification: Not enough information provided');
    reject(new Error('Not enough information provided'));
  } else {
    const uploadDate = new Date();
    // adatbazisba valo beszuras
    notificationDb.insertNotification(`${message}`, uploadDate, userId)
      .then((dbNotification) => {
        const notification = notificationMappers.toInsertNotificationDto(dbNotification);

        resolve(notification);
      })
      .catch((e) => {
        logger.level = 'error';
        logger.error('notificationService: insertNewNotification: Database error', e);
        reject(new Error('Database error'));
      });
  }
}));

// egy felhasznalonak szolo ertesitesek lekerese
module.exports.getNotificationsByUser = ((user) => new Promise((resolve, reject) => {
  if (!user) {
    logger.level = 'error';
    logger.error('notificationService: getNotificationsByUser: Not enough information provided');
    reject(new Error('Not enough information provided'));
  } else {
    notificationDb.getNotificationsByUser(user)
      .then((dbNotifications) => {
        const notifications = [];
        dbNotifications.forEach((dbNotification) => {
          notifications.push(notificationMappers.toNotificationDto(dbNotification));
        });
        resolve(notifications);
      })
      .catch((e) => {
        logger.level = 'error';
        logger.error('notificationService: getNotificationsByUser: Database error', e);
        reject(new Error('Database error'));
      });
  }
}));

// beallitjuk, hogy az adott user milyen ertesiteseket latott
module.exports.setNotificationsSeen = ((userId) => new Promise((resolve, reject) => {
  if (!userId) {
    logger.level = 'error';
    logger.error('notificationService: setNotificationsSeen: Not enough information provided');
    reject(new Error('Not enough information provided'));
  } else {
    notificationDb.setSeen(userId)
      .then(() => {
        resolve();
      })
      .catch(() => {
        logger.level = 'error';
        logger.error('notificationService: setNotificationsSeen: Database error');
        reject(new Error('Database error'));
      });
  }
}));
