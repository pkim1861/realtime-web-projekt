const express = require('express');
const commentService = require('../services/commentService');
const { verify } = require('../middlewares/loginMiddleware');

const router = express.Router();

router.get('/', verify, (req, res) => {
  commentService.getCommentsByBook(req.username)
    .then((books) => {
      res.status(200).json(books);
    })
    .catch((error) => {
      res.status(400).json({ error: error.message });
    });
});

module.exports = router;
