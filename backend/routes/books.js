const express = require('express');
const multer = require('multer');
const bookService = require('../services/bookService');
const commentService = require('../services/commentService');
const { verify } = require('../middlewares/loginMiddleware');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './uploads/');
  },
  filename(req, file, cb) {
    cb(null, `${Date.now()}.jpg`);
  },
});

// only accept images
const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({ storage, fileFilter });

const router = express.Router();

router.post('/', verify, upload.single('file'), (req, res) => {
  bookService.uploadNewBook(req.body, req.file)
    .then((book) => {
      res.status(201).json(book);
    })
    .catch((error) => {
      res.status(400).json({ error: error.message });
    });
});

router.get('/', verify, (req, res) => {
  bookService.getBooks(req.username)
    .then((books) => {
      res.status(200).json(books);
    })
    .catch((error) => {
      res.status(400).json({ error: error.message });
    });
});

router.get('/:bookId/comments', verify, (req, res) => {
  commentService.getCommentsByBook(req.params.bookId)
    .then((books) => {
      res.status(200).json(books);
    })
    .catch((error) => {
      res.status(400).json({ error: error.message });
    });
});

module.exports = router;
