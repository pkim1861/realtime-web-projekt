const express = require('express');
const multer = require('multer');
const userService = require('../services/userService');
const notificationService = require('../services/notificationService');
const chatService = require('../services/chatService');
const { verify } = require('../middlewares/loginMiddleware');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './uploads/');
  },
  filename(req, file, cb) {
    cb(null, `${Date.now()}.jpg`);
  },
});

// only accept images
const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({ storage, fileFilter });

const router = express.Router();

router.post('/', upload.single('file'), (req, res) => {
  userService.signUpNewUser(req.body, req.file)
    .then((user) => {
      res.status(201).json(user);
    })
    .catch((error) => {
      res.status(400).json({ error: error.message });
    });
});

router.get('/', verify, (req, res) => {
  userService.getAllUsers()
    .then((users) => {
      res.status(201).json(users);
    })
    .catch((error) => {
      res.status(400).json({ error: error.message });
    });
});

router.post('/login', (req, res) => {
  userService.login(req.body.user)
    .then((data) => {
      res.status(201).json({ token: data.token, username: data.email, userId: data.userId });
    })
    .catch((error) => {
      res.status(400).json({ error: error.message });
    });
});

router.post('/logout', verify, (req, res) => {
  res.sendStatus(200);
});

router.get('/:id', verify, (req, res) => {
  userService.getById(req.params.id, req.username)
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((error) => {
      res.status(400).json({ error: error.message });
    });
});

router.get('/:id/notifications', verify, (req, res) => {
  notificationService.getNotificationsByUser(req.params.id)
    .then((notifications) => {
      res.status(200).json(notifications);
    })
    .catch((error) => {
      res.status(400).json({ error: error.message });
    });
});

router.patch('/:id/notifications', verify, (req, res) => {
  notificationService.setNotificationsSeen(req.params.id)
    .then(() => {
      res.sendStatus(200);
    })
    .catch(() => {
      res.sendStatus(400);
    });
});

router.get('/:userId/channels/:channelId/messages', verify, (req, res) => {
  chatService.getChatMessages(req.params.userId, req.params.channelId)
    .then((messages) => {
      res.status(200).json(messages);
    })
    .catch((error) => {
      res.status(400).json(error);
    });
});

module.exports = router;
