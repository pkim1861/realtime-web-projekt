const db = require('../config/dbConfig');

module.exports.insertComment = (text, uploadDate) => db.let('source', (s) => {
  s.insert().into('comment')
    .set({
      text,
      uploadDate,
    });
})
  .commit()
  .one()
  .then((result) => result);

module.exports.createCommentLeavedBbyUser = (username, rid) => db.query(
  'CREATE EDGE commentLeavedByUser FROM (SELECT FROM user where username=:username) TO (SELECT FROM comment where @rid=:rid)',
  {
    params: {
      rid,
      username,
    },
  },
)
  .then(() => {
  });

module.exports.createCommentToBook = (commentId, bookId) => db.query(
  'CREATE EDGE commentToBook FROM (SELECT FROM comment where @rid=:commentId) TO (SELECT FROM book where @rid=:bookId)',
  {
    params: {
      commentId,
      bookId,
    },
  },
)
  .then(() => {
  });

module.exports.getComments = (bookId) => db.query(
  'select @rid as id, text, uploadDate, in(\'commentLeavedByUser\').image[0] as image, in(\'commentLeavedByUser\').firstName[0] as firstName, in(\'commentLeavedByUser\').lastName[0] as lastName from comment where out(\'commentToBook\').@rid[0]=:bookId order by uploadDate desc',
  {
    params: {
      bookId,
    },
  },

).then((comments) => comments);

module.exports.getCommentById = (commentId) => db.query(
  'select @rid as id, text, uploadDate, in(\'commentLeavedByUser\').image[0] as image, in(\'commentLeavedByUser\').firstName[0] as firstName, in(\'commentLeavedByUser\').lastName[0] as lastName, out(\'commentToBook\').in(\'bookUploadedByUser\').username[0] as recieverUser  from comment where @rid=:commentId',
  {
    params: {
      commentId,
    },
  },
).then((comment) => comment);
