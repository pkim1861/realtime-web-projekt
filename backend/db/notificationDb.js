const db = require('../config/dbConfig');

module.exports.getNotificationsByUser = (userId) => db.query(
  'select @rid as id, message, user, date, seen from Notification where user=:userId order by date desc',
  {
    params: {
      userId,
    },
  },

).then((notifications) => notifications);

module.exports.insertNotification = (message, date, user) => db.let('source', (s) => {
  s.insert().into('Notification')
    .set({
      message,
      date,
      user,
      seen: false,
    });
})
  .commit()
  .one()
  .then((result) => result);

exports.setSeen = (userId) => db.let('source', (s) => {
  s.update('Notification')
    .set({
      seen: true,
    })
    .where({ user: userId });
})
  .commit()
  .one()
  .then((result) => result);
