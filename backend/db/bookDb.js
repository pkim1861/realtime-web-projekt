const db = require('../config/dbConfig');

module.exports.insertBook = (title, author, uploadDate, image) => db.let('source', (s) => {
  s.insert().into('book')
    .set({
      title,
      author,
      uploadDate,
      image,
    });
})
  .commit()
  .one()
  .then((result) => result);

module.exports.createBookUploadedByUser = (username, rid) => db.query(
  'CREATE EDGE bookUploadedByUser FROM (SELECT FROM user where username=:username) TO (SELECT FROM book where @rid=:rid)',
  {
    params: {
      rid,
      username,
    },
  },
)
  .then(() => {
  });

exports.getBooks = (username) => db.query(
  `${`SELECT @rid as id, title, author, uploadDate, image, in('bookUploadedByUser')[0] as userId, in('bookUploadedByUser').firstName[0] as userFirstname, in('bookUploadedByUser').lastName[0] as userLastname, in('bookUploadedByUser').image[0] as userProfilePicture, eval("in('favorite').username contains '${username}'") as favorite, in('favorite').size() as favoriteNr from book`}`,
).then((books) => books);

exports.getBookById = (id, username) => db.query(
  `${`SELECT @rid as id, title, author, uploadDate, image, in('bookUploadedByUser')[0] as userId, in('bookUploadedByUser').username[0] as username, in('bookUploadedByUser').firstName[0] as userFirstname, in('bookUploadedByUser').lastName[0] as userLastname, in('bookUploadedByUser').image[0] as userProfilePicture, eval("in('favorite').username contains '${username}'") as favorite, in('favorite').size() as favoriteNr from book where @rid=${id}`}`,
).then((books) => books);

module.exports.getBookByUserId = (id, username) => db.query(
  `${`SELECT @rid as id, title, author, uploadDate, image, in('bookUploadedByUser').firstName[0] as userFirstname, in('bookUploadedByUser').lastName[0] as userLastname, in('bookUploadedByUser').image[0] as userProfilePicture, eval("in('favorite').username contains '${username}'") as favorite, in('favorite').size() as favoriteNr FROM book where in('bookUploadedByUser').@rid[0]=:id`}`,
  {
    params: {
      id,
    },
  },
).then((books) => books);

module.exports.createFavorite = (username, rid) => db.query(
  'CREATE EDGE favorite FROM (SELECT FROM user where username=:username) TO (SELECT FROM book where @rid=:rid)',
  {
    params: {
      rid,
      username,
    },
  },
)
  .then(() => {
  });

module.exports.dropFavorite = (username, rid) => db.query(
  'DELETE EDGE favorite FROM (SELECT FROM user where username=:username) TO (SELECT FROM book where @rid=:rid)',
  {
    params: {
      rid,
      username,
    },
  },
)
  .then(() => {
  });

module.exports.selectUserByBook = (bookId) => db.query(
  'select in(\'bookUploadedByUser\') as userId, author, title from book where @rid=:bookId',
  {
    params: {
      bookId,
    },
  },
)
  .then((username) => username);
