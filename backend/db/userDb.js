const db = require('../config/dbConfig');

module.exports.insertUser = (firstName, lastName, username, image, hash) => db.let('source', (s) => {
  s.insert().into('user')
    .set({
      firstName,
      lastName,
      username,
      image,
      password: hash,
    });
})
  .commit()
  .one()
  .then((result) => result);

module.exports.getUsers = (skip, limit) => db.query(
  'select @rid as id, username, firstName, lastName, image from user where SKIP :skip LIMIT :limit',
  {
    params: {
      skip,
      limit,
    },
  },
).then((users) => users);

module.exports.getUsersNr = () => db.query(
  'select count(*) as nr from user',
).then((nr) => nr);

module.exports.selectByUsername = (username) => db.query(
  'SELECT @rid as id, username, firstName, lastName, image FROM user where username=:uname',
  {
    params: {
      uname: username,
    },
  },
).then((user) => user);

module.exports.selectById = (rid) => db.query(
  'SELECT @rid as id, firstName, lastName, username, image FROM user where @rid=:rid',
  {
    params: {
      rid,
    },
  },
).then((user) => user);

module.exports.selectAll = () => db.query(
  'SELECT @rid as id, firstName, lastName, username, image FROM user',
).then((user) => user);

module.exports.selectUserNameAndPassword = (username) => db.query(
  'SELECT @rid as id, password FROM user where username=:uname',
  {
    params: {
      uname: username,
    },
  },
).then((data) => data);
