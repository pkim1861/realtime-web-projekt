const db = require('../config/dbConfig');

module.exports.insertChatMessage = (date, message, userFrom, userTo) => db.let('source', (s) => {
  s.insert().into('ChatRoom')
    .set({
      date,
      message,
      userFrom,
      userTo,
    });
})
  .commit()
  .one()
  .then((result) => result);

module.exports.getChatsByUser = (user1, user2) => db.query(
  'select @rid as id, message, userFrom, userFrom.firstName as userFromFirstName, userFrom.lastName as userFromLastName, userFrom.username as userFromUsername, userTo, userTo.firstName as userToFirstName, userTo.lastName as userToLastName, date from ChatRoom where (userFrom=:user1 and userTo=:user2) or (userTo=:user1 and userFrom=:user2) order by date desc',
  {
    params: {
      user1,
      user2,
    },
  },

).then((chats) => chats);

module.exports.getChatsById = (id) => db.query(
  'select @rid as id, message, userFrom, userFrom.firstName as userFromFirstName, userFrom.lastName as userFromLastName, userFrom.username as userFromUsername, userTo, userTo.firstName as userToFirstName, userTo.lastName as userToLastName, date from ChatRoom where @rid=:id',
  {
    params: {
      id,
    },
  },

).then((chat) => chat);
