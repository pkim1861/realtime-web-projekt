const jwt = require('jsonwebtoken');

// leellenorzi, hogy a felhasznalo be van-e jelentkezve
const verify = ((req, res, next) => {
  // headerben megkapja a jwt-t
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    const accessToken = req.headers.authorization.substring(7, req.headers.authorization.length);
    try {
      // ha sikerul dekodolni akkor valid
      const decoded = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
      const { email } = decoded;

      req.username = email;
      return next();
    } catch (e) {
      // ha problema volt akkor a felhasznalo nem jogosult, hogy lassa a tartalmat
      return res.status(401).json({ error: 'Unauthorized!' });
    }
  } else {
    // ha nem kapott headert akkor a felhasznalo nem jogosult, hogy lassa a tartalmat
    return res.status(401).json({ error: 'Unauthorized!' });
  }
});

module.exports.verify = verify;
